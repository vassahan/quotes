package ru.vadim.quotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CotirovkiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CotirovkiApplication.class, args);
    }

}
