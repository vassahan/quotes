package ru.vadim.quotes.exceptions;

public class QuoteIncorrecData {
    private String info;

    public QuoteIncorrecData() {
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
